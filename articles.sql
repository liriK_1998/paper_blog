-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 14 2019 г., 21:45
-- Версия сервера: 10.3.13-MariaDB
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `articles`
--

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `textofarticle` text NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `title`, `textofarticle`, `views`) VALUES
(1, 'Как написать демона на Node.js?', 'Никак!', 10),
(3, 'BlueKeep-2 — теперь уязвимы все новые версии Windows', 'Ещё не успела отшуметь уязвимость BlueKeep (CVE-2019-0708) для старых версий ОС Windows, нацеленная на реализацию протокола RDP, как снова пора ставить патчи. Теперь в зону поражения попали всё новые версии Windows. Если оценивать потенциальную угрозу от эксплуатации уязвимостей путем прямой атаки из интернета по методу WannaCry, то она актуальна для нескольких сотен тысяч хостов в мире и нескольких десятков тысяч хостов в России.\r\n\r\n<img src=\"https://i.pinimg.com/originals/3d/3d/d6/3d3dd612040e49c2ff1312b245fe4952.jpg\">\r\n\r\nПодробности и рекомендации по защите под катом.\r\n\r\nОпубликованные RCE-уязвимости в Службах Удаленных рабочих столов RDS в ОС Windows (CVE-2019-1181/1182) при успешной эксплуатации позволяют злоумышленнику, не прошедшему проверку подлинности, осуществить удаленное выполнение кода на атакуемой системе.\r\n\r\nДля эксплуатации уязвимостей злоумышленнику достаточно отправить специально сформированный запрос службе удаленных рабочих столов целевых систем, используя RDP (сам протокол RDP при этом не является уязвимым).\r\n\r\nВажно отметить, что любое вредоносное ПО, использующее эту уязвимость, потенциально может распространяться с одного уязвимого компьютера на другой аналогично распространению вредоносного ПО WannaCry по всему миру в 2017 году. Для успешной эксплуатации необходимо лишь иметь соответствующий сетевой доступ к хосту или серверу с уязвимой версией операционной системы Windows, в том числе, если системная служба опубликована на периметре.\r\n\r\nВерсии ОС Windows, подверженные уязвимости:\r\n\r\n    Windows 10 for 32-bit/x64-based\r\n    Windows 10 Version 1607 for 32-bit/x64-based Systems\r\n    Windows 10 Version 1703 for 32-bit/x64-based Systems\r\n    Windows 10 Version 1709 for 32-bit/x64-based Systems\r\n    Windows 10 Version 1709 for ARM64-based Systems\r\n    Windows 10 Version 1803 for 32-bit/x64-based Systems\r\n    Windows 10 Version 1803 for ARM64-based Systems\r\n    Windows 10 Version 1809 for 32-bit/x64-based Systems\r\n    Windows 10 Version 1809 for ARM64-based Systems\r\n    Windows 10 Version 1903 for 32-bit/x64-based Systems\r\n    Windows 10 Version 1903 for ARM64-based Systems\r\n    Windows 7 for 32-bit/x64-based Systems Service Pack 1\r\n    Windows 8.1 for 32-bit/x64-based systems\r\n    Windows RT 8.1\r\n    Windows Server 2008 R2 for Itanium-Based Systems Service Pack 1\r\n    Windows Server 2008 R2 for x64-based Systems Service Pack 1\r\n    Windows Server 2012\r\n    Windows Server 2012 R2\r\n    Windows Server 2016\r\n    Windows Server 2019\r\n\r\n\r\nРекомендуем:\r\n\r\n    Установить необходимые обновления для уязвимых ОС Windows, начиная с узлов на периметре и далее для всей инфраструктуры, согласно принятым в компании процедурам управления уязвимостями:\r\n    portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1181\r\n    portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1182\r\n    При наличии опубликованного сервиса RDP на внешнем периметре для уязвимой ОС – рассмотреть ограничение (закрытие) доступа до устранения уязвимостей.\r\n\r\n\r\nНа текущий момент нет информации о наличии PoC/эксплоита/эксплуатации данных уязвимостей, однако медлить с патчами не рекомендуем, часто их появление — вопрос нескольких суток.\r\n\r\nВозможные дополнительные компенсирующие меры:\r\n\r\n    Включение проверки подлинности на уровне сети (NLA). Однако уязвимые системы по-прежнему останутся уязвимыми для удаленного выполнения кода (RCE), если у злоумышленника есть действительные учетные данные, которые можно использовать для успешной аутентификации.\r\n    Временное выключение протокола RDP для уязвимых версий ОС до момента установки обновлений, использование альтернативных способов удаленного доступа к ресурсам.\r\n', 41);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
