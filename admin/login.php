<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<title>Login to admin panel</title>
</head>
<body>
	<header class="header">
    	<a href="/" class="logo">Бумажный_Блог</a>
  	</header>
	<form method="GET" action="/admin/panel.php">
		<input type="text" name="login" required placeholder="Login" class="login"><br/><br/>
		<input type="password" name="password" required placeholder="Password" class="passwd"><br/><br/>
		<button type="submit" class="btn"><font class="loginname">Log in</font></button>
	</form>
</body>
</html>